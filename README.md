# Flectra Community / website

None



Available addons
----------------

addon | version | summary
--- | --- | ---
[website_analytics_matomo](website_analytics_matomo/) | 2.0.1.0.0| Track website users using matomo
[website_crm_privacy_policy](website_crm_privacy_policy/) | 2.0.1.0.0| Website CRM privacy policy
[website_forum_subscription](website_forum_subscription/) | 2.0.1.0.0| Adds a button to allow subscription from the website
[website_google_tag_manager](website_google_tag_manager/) | 2.0.1.0.0| Add support for Google Tag Manager
[website_snippet_country_dropdown](website_snippet_country_dropdown/) | 2.0.1.0.0| Allow to select country in a dropdown
[website_odoo_debranding](website_odoo_debranding/) | 2.0.1.1.0| Remove Odoo Branding from Website
[website_menu_by_user_status](website_menu_by_user_status/) | 2.0.1.0.0| Allow to manage the display of website.menus
[website_no_crawler](website_no_crawler/) | 2.0.1.2.0| Disables robots.txt for indexing by webcrawlers like Google
[website_cookiefirst](website_cookiefirst/) | 2.0.1.1.0| Cookiefirst integration
[website_plausible](website_plausible/) | 2.0.1.0.1| Track website users using plausible
[website_crm_quick_answer](website_crm_quick_answer/) | 2.0.1.1.0| Add an automatic answer for contacts asking for info
[website_require_login](website_require_login/) | 2.0.1.0.1| Website Login Required
[website_google_analytics_4](website_google_analytics_4/) | 2.0.1.1.0| Google Analytics 4 integration
[website_legal_page](website_legal_page/) | 2.0.1.0.0| Website Legal Page



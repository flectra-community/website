# Copyright 2015 Therp BV <http://therp.nl>
# License AGPL-3.0 or later (https://www.gnu.org/licenses/agpl).

{
    "name": "Matomo analytics",
    "version": "2.0.1.0.0",
    "author": "Therp BV,Odoo Community Association (OCA)",
    "license": "AGPL-3",
    "category": "Website",
    "summary": "Track website users using matomo",
    "website": "https://gitlab.com/flectra-community/website",
    "depends": [
        "website",
    ],
    "data": [
        "views/res_config_settings.xml",
        "views/templates.xml",
    ],
    "installable": True,
}
